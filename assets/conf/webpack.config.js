var webpack = require('webpack');
var path = require('path');
var BundleTracker  = require('webpack-bundle-tracker');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var package = require('root-require')('./package.json');
console.log(package);

const APP_DIR = path.resolve(package.config.path.src);
const BUILD_DIR = path.resolve(package.config.path.build);
const PRODUCTION = process.env.NODE_ENV === "production";
const STYLESHEET_OUTPUT = "style.css";


var config = {
  context: __dirname,
  entry: {
    dev: 'webpack-dev-server/client?http://0.0.0.0:3000', // WebpackDevServer host and port
    hot: 'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
    app: APP_DIR,
  },

  output: {
    path: BUILD_DIR,
    publicPath: "",
    filename: "[name].js"
  },

  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
      disable: false,
      allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new BundleTracker({
      filename: path.join(package.config.path.conf, 'webpack.stats.json')
    })
  ],

  module: {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: /(node_modules|bower_components)/,
        include: APP_DIR,
        loaders: ['react-hot', 'babel']
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({ fallbackLoader: 'style-loader', loader: 'css-loader!sass-loader' })
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)(\?[a-z0-9=&.]*)?$/,
        loader: 'file-loader?name=assets/[name].[ext]'
      },
    ]
  },

  babel: package.babel,

  sassLoader: {
    data: "$env: " + process.env.NODE_ENV + ";",
    includePaths: [path.resolve(__dirname, "./assets/src/style")]
  },

  resolve: {
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.jsx', '.scss']
  },

  devtool: '#eval',
  devServer: {
    hot: true
  }
};

if(module.hot) {
  module.hot.accept();
  console.log("HOT UPDATE")
}

module.exports = config;
