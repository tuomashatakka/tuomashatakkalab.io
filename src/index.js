import axios from "axios"
import "../assets/font/style.scss"
import "./style.scss"



export const updateContent = (data) => {
  let el = document.getElementsByTagName('main')[0]
  el.innerHTML = data
}


export const fetchContent = (src) => {
  return axios
    .get(src)

    // Update the container whenever ready!
    .then(res => {
      updateContent(res.data)
    })

    // Display the error if not found
    .catch(error => {
      let err = error.response ? error.response.data : 'Error: ' + error.message
      updateContent(err)
    })
}


export const onLocationChange = (src, event) => {

  // Start fetching the content
  fetchContent(src)

  // Briefly show the url while the data is being loaded
  updateContent("Loading data from" + src)
}


export const changeLocation = (ref, event) => {

  let title = "Tuomas Hatakka"
  let href = ref.target || ref
  if (href.href) { href = href.href }
  if (href.getAttribute) {
      href = href.getAttribute('href') ||
      href.getAttribute('data-target')
  }

  history.pushState({page: 1}, title, href)
  onLocationChange(href, event)
}

export const overrideLinkHandlers = () => {
  let links = document.getElementsByTagName('a')

  for(let n in links) {
    let link = links[n]

    if(link.addEventListener) {
      link.addEventListener('click', event => {

        try {
          if(event.preventDefault) event.preventDefault()
          return changeLocation(link, event)
        }
        catch(e) { console.log(e) }
      })
    }
  }
}


let {location} = document
window.onpopstate = (e) => onLocationChange(location, e.state)
document.onreadystatechange = () => {
  console.log("Document ready, replacing links' actions")
  overrideLinkHandlers()
}

window.app = module.exports
