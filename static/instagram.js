

async function main () {
  const accessToken     = location.hash.replace("#access_token=", "")
  const accessTokenNode = document.createElement("div")
  const response = await fetch("https://api.instagram.com/v1/users/self/?access_token=" + accessToken)
  const content  = await response.json()

  updateProfileNode(content.data)
}


function updateProfileNode (data) {
  const element = document.querySelector('#user-profile') || document.createElement('div')


  element.setAttribute('id', 'user-profile')
  element.innerHTML = `


    <div class='row'>
      <img src='${data.profile_picture}' />
      <div class='stack'>
        <h2>
          <span class='username'>${data.username}</span>
          <sub>${data.full_name}</sub>
        </h2>

        <dl class='grid'>

          <section>
            <dt>Media</dt>
            <dd>${data.counts.media}</dd>
          </section>

          <section>
            <dt>Followers</dt>
            <dd>${data.counts.followed_by}</dd>
          </section>

          <section>
            <dt>Following</dt>
            <dd>${data.counts.follows}</dd>
          </section>

        </dl>
      </div>
    </div>

    <p>${data.bio}</p>
  `

  console.log(data)
  document.getElementById('main').appendChild(element)
  document.getElementById('authorize').remove()
}


const hasToken = location.hash.startsWith("#access_token=")
if (hasToken)
  main()
else
  updateProfileNode({
    "id":"47785559",
    "username":"tunpi.bmp",
    "profile_picture":"https://scontent.cdninstagram.com/vp/b5229ba26a297cb6886bfb272db6db5c/5D23C201/t51.2885-19/s150x150/46937777_199907784291801_6893658286604681216_n.jpg?_nc_ht=scontent.cdninstagram.com",
    "full_name":"TUOM∆S∧HATAKK∆",
    "bio":"⊛\temo ⟋ scene ⟋ 4life ∧ for good\n⊗\tall things bright ∧ colorful\n⊚\tsouthern finland\n⎋ \tsnap\ttunp3\n★ lol",
    "website":"",
    "is_business":false,
    "counts": {
      "media":197,
      "follows":597,
      "followed_by":327
    }
  })
